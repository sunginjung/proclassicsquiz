package edu.uchicago.jung.proclassicsquiz;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class QuestionActivity extends AppCompatActivity {

    private static final String DELIMITER = "\\|";
    private static final int NUM_ANSWERS = 5;
    private static final int ENGLISH = 0;
    private static final int SHANSKRIT = 1;
    private static final int GREEK = 2;
    private static final int MIXED = 3;

    private int mQuestionType = 0;

    private Random mRandom;

    private Question mQuestion;
    private String[] mWordsTranslations;
    private boolean mItemSelected = false;
    //make these members
    private TextView mQuestionNumberTextView;
    private RadioGroup mQuestionRadioGroup;
    private TextView mQuestionTextView;
    private Button mSubmitButton;
    private Button mQuitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //figure out which option user chose in the main screen
        mQuestionType = MainActivity.getQuestionType();

        mWordsTranslations = getResources().getStringArray(R.array.words_translations);

        //get refs to inflated members
        mQuestionNumberTextView = (TextView) findViewById(R.id.questionNumber);
        mQuestionTextView = (TextView) findViewById(R.id.questionText);
        mSubmitButton = (Button) findViewById(R.id.submitButton);

        //init the random
        mRandom = new Random();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });


        //set quit button action
        mQuitButton = (Button) findViewById(R.id.quitButton);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayResult();
            }
        });

        mQuestionRadioGroup = (RadioGroup) findViewById(R.id.radioAnswers);
        //disallow submitting until an answer is selected
        mQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mSubmitButton.setEnabled(true);
                mItemSelected = true;
            }
        });

        fireQuestion();

    }

    private void submit() {

        Button checkedButton = (Button) findViewById(mQuestionRadioGroup.getCheckedRadioButtonId());
        String guess = checkedButton.getText().toString();
        //see if they guessed right
        if (mQuestionType == SHANSKRIT) {
            if (mQuestion.getSanskrit().equals(guess)) {
                QuizTracker.getInstance().answeredRight();
            } else {
                QuizTracker.getInstance().answeredWrong();
            }
            if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
                //increment the question number
                QuizTracker.getInstance().incrementQuestionNumber();
                fireQuestion();
            } else {
                displayResult();
            }
        } else if (mQuestionType == GREEK) {
            if (mQuestion.getGreek().equals(guess)) {
                QuizTracker.getInstance().answeredRight();
            } else {
                QuizTracker.getInstance().answeredWrong();
            }
            if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
                //increment the question number
                QuizTracker.getInstance().incrementQuestionNumber();
                fireQuestion();
            } else {
                displayResult();
            }

        }
        else{
            if (mQuestion.getSanskrit().equals(guess) || mQuestion.getGreek().equals(guess)) {
                QuizTracker.getInstance().answeredRight();
            } else {
                QuizTracker.getInstance().answeredWrong();
            }
            if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
                //increment the question number
                QuizTracker.getInstance().incrementQuestionNumber();
                fireQuestion();
            } else {
                displayResult();
            }

        }

    }
    private Question getQuestion() {
        //generate corr answer
        ArrayList<String> strAnswers = getRandomWordTranslation(mQuestionType);
        if(mQuestionType == SHANSKRIT){
            mQuestion = new Question(strAnswers.get(0), strAnswers.get(1), null);
        }else if(mQuestionType == GREEK){
            mQuestion = new Question(strAnswers.get(0), null, strAnswers.get(1));
        }else{
            //if mixed is chosen
            mQuestion = new Question(strAnswers.get(0), strAnswers.get(1), strAnswers.get(2));
        }

        //generates 5 wrong answers
        while (mQuestion.getWrongAnswers().size() < NUM_ANSWERS) {
            ArrayList<String> strWordTranslations = getRandomWordTranslation(mQuestionType);

            if(mQuestionType == SHANSKRIT) {
                //if the one we picked is equal to the answer OR
                //if is not from the same region as the answer OR
                //if we already picked this one
                while (strWordTranslations.get(1).equals(strAnswers.get(1)) ||
                        mQuestion.getWrongAnswers().contains(strWordTranslations.get(1))) {
                    //then we need pick another one
                    strWordTranslations = getRandomWordTranslation(mQuestionType);
                }
                mQuestion.addWrongAnswer(strWordTranslations.get(1));
            } else if(mQuestionType == GREEK) {
                while (strWordTranslations.get(1).equals(strAnswers.get(1)) ||
                        mQuestion.getWrongAnswers().contains(strWordTranslations.get(1))) {
                    //then we need pick another one
                    strWordTranslations = getRandomWordTranslation(mQuestionType);
                }
                mQuestion.addWrongAnswer(strWordTranslations.get(1));

            } else {
                //if mixed option is chosen, 7 answer choices are generated in this case for complexity
                while ((strWordTranslations.get(1).equals(strAnswers.get(1)) &&
                        strWordTranslations.get(2).equals(strAnswers.get(2))) ||
                        strWordTranslations.get(0).equals(strAnswers.get(0)) ||
                        mQuestion.getWrongAnswers().contains(strWordTranslations.get(1))) {
                    //then we need pick another one
                    strWordTranslations = getRandomWordTranslation(mQuestionType);
                }
                mQuestion.addWrongAnswer(strWordTranslations.get(1));
                mQuestion.addWrongAnswer(strWordTranslations.get(2));
            }
        }
        return mQuestion;
    }

    //depending of the option user chose, the question excludes either GREEK or SANSKRIT.
    //if Mixed is chosen, all words are included
    private ArrayList<String> getRandomWordTranslation(int questionType) {
        int index = mRandom.nextInt(mWordsTranslations.length);
        ArrayList<String> questionArray = new ArrayList<String>();

        //the index for the arraylist defers according to the question type chosen by the user
        if(questionType == SHANSKRIT) {
            String[] tempArray = mWordsTranslations[index].split(DELIMITER);
            questionArray.add(tempArray[0]);
            questionArray.add(tempArray[1]);
            return questionArray;
        } else if(questionType == GREEK){
            String[] tempArray = mWordsTranslations[index].split(DELIMITER);
            questionArray.add(tempArray[0]);
            questionArray.add(tempArray[2]);
            return questionArray;
        } else{
            String[] tempArray = mWordsTranslations[index].split(DELIMITER);
            questionArray.add(tempArray[0]);
            questionArray.add(tempArray[1]);
            questionArray.add(tempArray[2]);
            return questionArray;
        }
    }

    private void populateUserInterface() {
        //take care of button first
        mSubmitButton.setEnabled(false);
        mItemSelected = false;

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        //will generate a number 0-4 inclusive
        int randomPosition = mRandom.nextInt(NUM_ANSWERS);
        int counter = 0;
        mQuestionRadioGroup.removeAllViews();

        if(mQuestionType == SHANSKRIT) {
            //for each of the 5 wrong answers
            for (String wrongAnswer : mQuestion.getWrongAnswers()) {
                if (counter == randomPosition) {
                    //insert the cor answer
                    addRadioButton(mQuestionRadioGroup, mQuestion.getSanskrit());
                } else {
                    addRadioButton(mQuestionRadioGroup, wrongAnswer);
                }
                counter++;
            }
        } else if(mQuestionType == GREEK){
            for (String wrongAnswer : mQuestion.getWrongAnswers()) {
                if (counter == randomPosition) {
                    //insert the cor answer
                    addRadioButton(mQuestionRadioGroup, mQuestion.getGreek());
                } else {
                    addRadioButton(mQuestionRadioGroup, wrongAnswer);
                }
                counter++;
            }
        } else{
            for (String wrongAnswer : mQuestion.getWrongAnswers()) {
                if (counter == randomPosition) {
                    //insert the cor answer (randomly chosen between Greek and Sanskrit)
                    int tempInt = mRandom.nextInt(2);
                    if(tempInt == 0){
                        addRadioButton(mQuestionRadioGroup, mQuestion.getGreek());
                    } else {
                        addRadioButton(mQuestionRadioGroup, mQuestion.getSanskrit());
                    }
                } else {
                    addRadioButton(mQuestionRadioGroup, wrongAnswer);
                }
                counter++;
            }
        }
    }

    private void addRadioButton(RadioGroup questionGroup, String text) {
        RadioButton button = new RadioButton(this);
        button.setText(text);
        button.setTextColor(Color.WHITE);
        button.setButtonDrawable(android.R.drawable.btn_radio);
        questionGroup.addView(button);

    }

    private void fireQuestion(){
        mQuestion = getQuestion();
        populateUserInterface();
    }

    private void displayResult(){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
        finish();
    }

    //menu for the quiz view
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuQuit:

                displayResult();
                return true;
            case R.id.menuSubmit:
                if(mItemSelected){

                    submit();
                }
                else{
                    Toast toast = Toast.makeText(this, getResources().getText(R.string.pleaseSelectAnswer), Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
