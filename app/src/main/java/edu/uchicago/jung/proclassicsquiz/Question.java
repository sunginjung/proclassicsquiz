package edu.uchicago.jung.proclassicsquiz;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by sunginjung on 4/7/17.
 */

public class Question {

    private String mEnglish;
    private String mSanskrit;
    private String mGreek;
    private Set<String> mWrongAnswers = new HashSet<String>();

    public Question(String english, String latin, String greek) {
        this.mEnglish = english;
        this.mSanskrit = latin;
        this.mGreek = greek;
    }

    public String getSanskrit() {
        return mSanskrit;
    }

    public String getGreek() {
        return mGreek;
    }

    public Set<String> getWrongAnswers() {
        return mWrongAnswers;
    }

    public boolean addWrongAnswer(String wrongAnswer){
        return mWrongAnswers.add(wrongAnswer);
    }

    public String getQuestionText(){
        return "What is the translation of " + mEnglish + "?";
    }
}
