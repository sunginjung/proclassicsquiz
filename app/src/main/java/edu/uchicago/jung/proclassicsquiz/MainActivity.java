package edu.uchicago.jung.proclassicsquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //to determine the mix of questions to generate
    private static final int SANSKRIT = 1;
    private static final int GREEK = 2;
    private static final int MIXED = 3;
    private static int mQuestionType = 0;

    //all the buttons and views
    private Button mSanskritButton, mGreekButton, mMixedButton, mExitButton;
    private String mName;
    private EditText mNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //define  all the buttons and editText
        mSanskritButton = (Button) findViewById(R.id.sanskritButton);
        mGreekButton = (Button) findViewById(R.id.greekButton);
        mMixedButton = (Button) findViewById(R.id.mixedButton);
        mExitButton = (Button) findViewById(R.id.exitButton);
        mNameEditText = (EditText) findViewById(R.id.editName);

        //set listeners for the buttons and editText
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSanskritButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startQuiz(SANSKRIT);
            }
        });

        mGreekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startQuiz(GREEK);
            }
        });

        mMixedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startQuiz(MIXED);
            }
        });

    }

    //quiz depends on the user's choice
    private void startQuiz(int questionType){
        mName = mNameEditText.getText().toString();
        //if user forgot to enter his/her name
        if (mName.equals("")){
            Toast.makeText(this, "Please enter your name first!!!", Toast.LENGTH_SHORT).show();
            return;
        } else {
            QuizTracker.getInstance().setName(mName);
            askQuestion(1, questionType);
        }
    }

    private void askQuestion(int number, int questionType) {
        mQuestionType = questionType;
        QuizTracker.getInstance().setQuestionNum(number);
        Intent intent = new Intent(MainActivity.this, QuestionActivity.class);
        startActivity(intent);
    }

    //to inform which language the user selected
    public static int getQuestionType(){
        return mQuestionType;
    }


    //create menut for the main view
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuExit:
                finish();
                return true;
            case R.id.menuSanskrit:
                startQuiz(SANSKRIT);
                return true;
            case R.id.menuGreek:
                startQuiz(GREEK);
                return true;
            case R.id.menuMixed:
                startQuiz(MIXED);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
